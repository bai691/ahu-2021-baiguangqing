package com.bai691.ahu_2021_demo.controller;

import com.bai691.ahu_2021_demo.bean.calc_record;
import com.bai691.ahu_2021_demo.service.calc_record_service;
import com.bai691.ahu_2021_demo.service.calc_record_service_impl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 前端控制器
 * @author: Bai Guangqing
 * @version: 1.0
 */

@RestController // 返回 JSON 格式
public class controller_test {
    @GetMapping("/add") // servlet URL映射
    public double add(@RequestParam("a") double a,@RequestParam("b") double b){
        // 生成记录
        calc_record record = new calc_record();
        record.setOperand1(a);
        record.setOperand2(b);
        record.setOperator("+");
        record.setResult(a+b);
        // 记录传输
        boolean flag = false;
        calc_record_service recordService = new calc_record_service_impl();
        flag = recordService.add(record);

        return a+b;// 显示结果
    }
    @GetMapping("/sub") // servlet URL映射
    public double sub(@RequestParam("a") double a,@RequestParam("b") double b){
        // 生成记录
        calc_record record = new calc_record();
        record.setOperand1(a);
        record.setOperand2(b);
        record.setOperator("-");
        record.setResult(a-b);
        // 记录传输
        boolean flag = false;
        calc_record_service recordService = new calc_record_service_impl();
        flag = recordService.add(record);

        return a-b;// 显示结果
    }
    @GetMapping("/multiply") // servlet URL映射
    public double mul(@RequestParam("a") double a,@RequestParam("b") double b){
        // 生成记录
        calc_record record = new calc_record();
        record.setOperand1(a);
        record.setOperand2(b);
        record.setOperator("*");
        record.setResult(a*b);
        // 记录传输
        boolean flag = false;
        calc_record_service recordService = new calc_record_service_impl();
        flag = recordService.add(record);

        return a*b;// 显示结果
    }
    @GetMapping("/division") // servlet URL映射
    public String div(@RequestParam("a") double a,@RequestParam("b") double b){
        if(b == 0)// 除0错误
            return "Divide By Zero";
        // 生成记录
        calc_record record = new calc_record();
        record.setOperand1(a);
        record.setOperand2(b);
        record.setOperator("/");
        record.setResult(a/b);
        // 记录传输
        boolean flag = false;
        calc_record_service recordService = new calc_record_service_impl();
        flag = recordService.add(record);

        return ""+(a/b);//显示结果
    }
}
