package com.bai691.ahu_2021_demo.dao;

import com.bai691.ahu_2021_demo.bean.calc_record;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @description: 数据操作实现类
 * @author: Bai Guangqing
 * @version: 1.0
 */

public class calc_record_dao_impl implements calc_record_dao{

    @Override
    public int add_record(Connection connection, calc_record record) throws Exception {
        PreparedStatement ps = null;
        int flag = 0;

        if(null != connection){
            String sql = "insert into calc_record (operand1,operator,operand2,result)" +
                    "values(?,?,?,?)";
            Object[] params = {record.getOperand1(),record.getOperator(),record.getOperand2(),record.getResult()};
            flag = baseDAO.execute(connection,ps,sql,params);// 执行
            baseDAO.closeResource(null,ps,null);// 关闭
        }
        return flag;
    }
}
