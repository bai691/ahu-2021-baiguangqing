package com.bai691.ahu_2021_demo.bean;
/**
 * @description: 记录实体类
 * @author: Bai Guangqing
 * @version: 1.0
 */
public class calc_record {
    private int id;
    private double operand1;// 操作数1
    private double operand2;// 操作数2
    private String operator;// 运算符
    private double result;  // 结果
    // Getter & Setter
    public double getOperand1() {
        return operand1;
    }

    public void setOperand1(double operand1) {
        this.operand1 = operand1;
    }

    public double getOperand2() {
        return operand2;
    }

    public void setOperand2(double operand2) {
        this.operand2 = operand2;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }
}
