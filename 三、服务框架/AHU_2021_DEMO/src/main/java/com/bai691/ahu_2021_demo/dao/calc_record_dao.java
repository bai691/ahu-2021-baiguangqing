package com.bai691.ahu_2021_demo.dao;

import com.bai691.ahu_2021_demo.bean.calc_record;

import java.sql.Connection;

/**
 * @description: 数据操作接口
 * @author: Bai Guangqing
 * @version: 1.0
 */

public interface calc_record_dao {
    public int add_record(Connection connection, calc_record record) throws Exception;
}
