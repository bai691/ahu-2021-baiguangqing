package com.bai691.ahu_2021_demo.service;

import com.bai691.ahu_2021_demo.bean.calc_record;

/**
 * @description: 保存服务接口
 * @author: Bai Guangqing
 * @version: 1.0
 */

public interface calc_record_service {
    public boolean add(calc_record record);
}
