package com.bai691.ahu_2021_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: 启动类
 * @author: Bai Guangqing
 * @version: 1.0
 */

@SpringBootApplication
public class Ahu2021DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ahu2021DemoApplication.class, args);
    }

}
