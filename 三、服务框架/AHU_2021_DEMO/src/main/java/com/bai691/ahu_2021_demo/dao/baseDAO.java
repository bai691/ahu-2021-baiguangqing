package com.bai691.ahu_2021_demo.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * @description: 数据库操作封装类
 * @author: Bai Guangqing
 * @version: 1.0
 */

public class baseDAO {
    static {
        init();
    }
    // 相关参数
    private static String driver;
    private static String url;
    private static String user;
    private static String password;
    // 连接初始化
    public static void init(){
        // 读取配置文件
        Properties params = new Properties();
        String configFile = "mysql.properties";
        // 加载
        try {
            params.load(new FileInputStream(configFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 初始化参数
        user = params.getProperty("user");
        driver = params.getProperty("driver");
        url = params.getProperty("url");
        password = params.getProperty("password");

    }
    // 建立连接
    public static Connection getConnection(){
        Connection connection = null;

        try {
            Class.forName(driver);// 驱动注册
            connection = DriverManager.getConnection(url,user,password);// 建立连接
        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }
    // 执行SQL语句
    public static int execute(Connection connection, PreparedStatement ps,String sql,Object[] params) throws Exception{
        int updateRows = 0;// 变更行数
        ps = connection.prepareStatement(sql);
        // 占位符赋值
        for(int i = 0;i < params.length;i++)
            ps.setObject(i+1,params[i]);
        // 执行
        updateRows = ps.executeUpdate();
        return updateRows;
    }
    // 关闭资源
    public static boolean closeResource(Connection connection, PreparedStatement ps, ResultSet rs) {
        boolean flag = true;
        if (rs != null) {
            try {
                rs.close();
                rs = null;//GC回收
            } catch (SQLException e) {
                e.printStackTrace();
                flag = false;
            }
        }
        if (ps != null) {
            try {
                ps.close();
                ps = null;//GC回收
            } catch (SQLException e) {
                e.printStackTrace();
                flag = false;
            }
        }
        if (connection != null) {
            try {
                connection.close();
                connection = null;//GC回收
            } catch (SQLException e) {
                e.printStackTrace();
                flag = false;
            }
        }

        return flag;
    }
}
