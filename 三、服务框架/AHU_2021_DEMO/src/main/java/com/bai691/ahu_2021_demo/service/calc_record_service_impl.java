package com.bai691.ahu_2021_demo.service;

import com.bai691.ahu_2021_demo.bean.calc_record;
import com.bai691.ahu_2021_demo.dao.baseDAO;
import com.bai691.ahu_2021_demo.dao.calc_record_dao;
import com.bai691.ahu_2021_demo.dao.calc_record_dao_impl;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @description: 保存服务实现类
 * @author: Bai Guangqing
 * @version: 1.0
 */

public class calc_record_service_impl implements calc_record_service {
    private calc_record_dao recordDAO;

    public calc_record_service_impl(){
        recordDAO = new calc_record_dao_impl();
    }

    @Override
    public boolean add(calc_record record) {
        boolean flag = false;
        Connection connection = null;

        try {
            connection = baseDAO.getConnection();// 建立连接
            connection.setAutoCommit(false);// 关闭自动提交
            if(recordDAO.add_record(connection,record) > 0)// 添加记录
                flag = true;
            connection.commit();// 提交
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();// 回滚
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }finally {
            baseDAO.closeResource(connection,null,null);// 关闭资源
        }
        return flag;
    }
}
