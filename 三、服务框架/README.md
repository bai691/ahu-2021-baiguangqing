####1.采用SpringBoot框架搭建
####2.实现细节
#####1.项目结构
1.工程启动类：Ahu2021DemoApplication.java
2.实体类：Bean
3.数据访问层：DAO
4.服务层：Service
5.前端控制器：Controller
6.数据库：Mysql
#####2.思路
读取接口参数，传递给后台进行相应运算，同时将相关数据写入数据库
####3.测试
step 1.启动SpringBoot服务
step 2.浏览器键入url请求
eg. http://localhost:8080/add?a=3&b=6
step 3.页面显示结果，数据库中存入记录