create database ahu_2021_test;

use ahu_2021_test;

create table calc_record (
	id int not null auto_increment comment '主键',
	operand1 double not null comment '运算数1',
	operator varchar(1) not null comment '运算符',
	operand2 double not null comment '运算数2',
	result double not null comment '运算结果',
	primary key (id)
);