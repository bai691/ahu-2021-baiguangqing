/*************************************************  
Author: Bai Guangqing
Date:2021-10-10 
Description:C++ 简易计算器 
**************************************************/ 
#include<iostream>
using namespace std;

class calculator{// 计算器类
private:
    double operand1;// 操作数1
    double operand2;// 操作数2
    char operator_; // 运算符
    double result;  // 结果
public:
    void calculate(){
        cin>>operand1>>operator_>>operand2;// 读取输入
        switch(operator_){// 判断运算类型
            case '+':// 加法
                result = operand1 + operand2;
                cout<< result <<endl;
                break;
            case '-':// 减法
                result = operand1 - operand2;
                cout<< result <<endl;
                break;
            case '*':// 乘法
                result = operand1 * operand2;
                cout<< result <<endl;
                break;
            case '/':// 除法
                if(operand2 != 0){
                    result = operand1/operand2;
                    cout<< result <<endl;
                }else// 除零判断
                    cout<<"/ by zero!"<<endl;
                break;
            default:// 无效输入
                cout<<"Unvalid Format!"<<endl;    
        }
    }
};

int main(){// 测试
    calculator c;
    c.calculate();
    return 0;
}